﻿LibDiscount

Bienvenue et merci d'avoir choisi le site web LibDiscount.

Ce guide vous montrera comment profiter au mieux du site.


|******* La Base de données *******|
------------------------------------
Avant toute chose, il vous faudra créer une base de données.
Pour cela, la plate-forme d'administration de base de données PGAdmin couplée au logiciel DBeaver pourront être de bons candidats.
Avec PGAdmin, faites un clic droit sur Databases > Create > Database et appelez la base "libdiscount".
Avec DBeaver, vous pouvez écrire la requête : CREATE DATABASE libdiscount;

Le site se chargera de créer les tables lors d'un premier enregistrement.



|******* Le profil administrateur *******|
------------------------------------------
Une fois la base de données créée, lancez le site.
Allez sur la page admin.jsp (cda20156-libdiscount-bc.git/admin.jsp) et entrez comme identifiants :
Email : admin@test.com
Mot de passe : password
Le site créera alors le reste de la base de données ainsi que le profil administrateur, un profil utilisateur et une annonce de test.
Vous serez également connecté au site.


|******* Le site en tant qu'administrateur *******|
---------------------------------------------------
De par votre rôle d'administrateur, vous avec la possibilité de :

- Voir toutes les annonces du site.
	Vous pouvez voir chaque annonce en détail en cliquant sur le lien "Voir l'annonce". 	
	Une fois sur la fiche annonce, vous pouvez la supprimer en cliquant sur le lien "Supprimer l'annonce". Cela l'effacera définitivement de la base de données.

- Voir tous les utilisateurs enregistrés sur le site.
	En cliquant sur "Voir le profil", vous aurez accès aux informations détaillées de l'utilisateur.
	Il est possible de désactiver un utilisateur en cliquant sur le lien "Désactiver l'utilisateur". Le compte concerné ne pourra plus se connecter au site.


|******* Le site en tant qu'utilisateur *******|
------------------------------------------------
Les utilisateurs doivent créer un compte sur la page d'accueil avant de pouvoir accéder au site. Une fois cette étape validée, ils peuvent :

- Créer une annonce : une fois créée, celle-ci est activée et visible sur le site.
- Voir toutes leurs annonces : en cliquant sur "Voir l'annonce", ils auront accès aux détails et pourront modifier ou désactiver l'annonce en cliquant sur les liens appropriés.


|******* Le site pour tous les roles *******|
---------------------------------------------
Les utilisateurs et l'admin peuvent voir leur profil et rechercher des annonces via des mots-clés dans le formulaire de recherche disponible à tout moment.
Il est possible de chercher une annonce via le titre du livre, le type/genre littéraire, la ville de l'annonceur et le niveau scolaire des livres.