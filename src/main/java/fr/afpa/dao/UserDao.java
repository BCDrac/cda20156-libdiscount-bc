package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.User;
import fr.afpa.session.HibernateUtils;

/**
 * DAO utilisateur.
 * 
 * Gère les requêtes en base de données concernant l'entité User.
 * 
 * @author Cécile
 */
public class UserDao {
	
	
	/**
	 * Vérification d'un email.
	 * 
	 * Vérifie, lorsque un utilisateur crée un compte, que l'email entré n'est pas déjà utilisé.
	 * 
	 * @param email l'email à vérifier
	 * @return boolean : return true si l'email est déjà présent en base de données
	 */
	public boolean isEmailUsed(String email) {

		boolean isUsed = false;
		Session session = HibernateUtils.getSession();

		try {	
			String emailFound = (String) session
					.createQuery("SELECT user.email FROM User user WHERE user.email = :email")
					.setParameter("email", email).uniqueResult();

			if (emailFound.equals(email)) {
				isUsed = true;
			}

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return isUsed;
	}	
	
	/**
	 * Insère un utilisateur en base de données.
	 * 
	 * @param user l'objet utilisateur à insérer en base de données
	 * @return boolean : return true si l'insertion s'est bien effectuée
	 */
	public boolean insertUser(User user) {
		
		boolean transactSuccess = false;
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			session.persist(user);
			transact.commit();
			transactSuccess = true;

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return transactSuccess;
	}
	
	
	/**
	 * Récupère un utilisateur d'après l'email et le mot de passe fournis lors de l'identification
	 * 
	 * @param email l'email utilisateur servant pour l'identification
	 * @param password le mot de passe servant pour l'identification
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getUser(String email, String password) {

		User user = null;
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT user "
					+ "FROM User user "
					+ "WHERE user.email = :email "
					+ "AND user.password = :password "
					+ "AND user.activated = true "
					+ "AND user.role = 'user'"
			);
			query.setParameter("email", email);
			query.setParameter("password", password);
			user = (User) query.getSingleResult();
		} catch (Exception e) {

		} finally {
			session.close();
		}
		return user;
	}
	
	
	/**
	 * Récupère un utilisateur d'après l'email et le mot de passe fournis lors de l'identification
	 * 
	 * @param email l'email utilisateur servant pour l'identification
	 * @param password le mot de passe servant pour l'identification
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getAdmin(String email, String password) {

		User user = null;
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT user "
					+ "FROM User user "
					+ "WHERE user.email = :email "
					+ "AND user.password = :password "
					+ "AND user.activated = true "
					+ "AND user.role = 'admin'"
			);
			query.setParameter("email", email);
			query.setParameter("password", password);
			user = (User) query.getSingleResult();
		} catch (Exception e) {

		} finally {
			session.close();
		}
		return user;
	}
	
	
	/**
	 * Récupère un utilisateur d'après son id
	 * 
	 * @param idUser l'id servant à récupérer l'utilisateur
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getUserById(int idUser) {
		
		User user = null;
		Session session = HibernateUtils.getSession();

		try {
			user = session.get(User.class, idUser);
		
		} catch (Exception e) {

		} finally {
			session.close();
		}
		return user;
	}
	
	
	/**
	 * Récupère tous les utilisateurs de la base de données
	 * 
	 * @return List : une liste contenant tous les utilisateurs enregistrés sur le site
	 */
	public List<User> getAllUsers(){
		
		List<User> list = new ArrayList<User>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT user "
					+ "FROM User user "
					+ "WHERE user.role = 'user'"
			);
			list = query.getResultList();
		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Désactive un utilisateur, passant son état "activated" à false.
	 * 
	 * @param idUser l'id utilisateur à désactiver
	 */
	public void disableUser(int idUser) {
		
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			User user = session.get(User.class, idUser);
			
			if (user != null) {
				user.setActivated(false);
				session.update(user);
				transact.commit();
			}

		} catch (Exception e) {

		} finally {
			session.close();
		}
	}
	
}