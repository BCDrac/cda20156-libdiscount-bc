package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Advertisement;
import fr.afpa.session.HibernateUtils;

/**
 * DAO annonce.
 * 
 * Gère les requêtes en base de données concernant l'entité Advertisement
 * 
 * @author Cécile
 */
public class AdvertisementDao {
		
	
	/**
	 * Insère une annonce en base de données
	 * 
	 * @param advert l'annonce à insérer
	 * @return boolean : retourne true si l'annonce a bien été insérée
	 */
	public boolean insertAdvertisement(Advertisement advert) {
		
		boolean transactSuccess = false;
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			session.persist(advert);
			transact.commit();
			transactSuccess = true;

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return transactSuccess;
	}
	
	
	/**
	 * Met à jour une annonce en base de données
	 * 
	 * @param advert l'annonce à mettre à jour
	 */
	public void updateAdvertisement(Advertisement advert) {
		
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			session.update(advert);
			System.out.println("ID: " + advert.getId() + " titre : " + advert.getTitle());
			transact.commit();

		} catch (Exception e) {

		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Sélectionne une annonce par son id et activée
	 * 
	 * @param idAdvert l'id de l'annonce
	 * @return Advertisement : renvoie l'annonce recherchée
	 */
	public Advertisement getAdvertByIdActivated(int idAdvert) {
		Advertisement advert = null;
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
					+ "FROM Advertisement advert "
					+ "WHERE advert.id = :idAdvert "
					+ "AND advert.activated = true"
			);
			query.setParameter("idAdvert", idAdvert);
			advert = (Advertisement) query.getSingleResult();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return advert;
	}
	
	
	/**
	 * Sélectionne une annonce par son id peut importe son activation
	 * 
	 * @param idAdvert l'id de l'annonce
	 * @return Advertisement : renvoie l'annonce recherchée
	 */
	public Advertisement getAdvertById(int idAdvert) {
		Advertisement advert = null;
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
					+ "FROM Advertisement advert "
					+ "WHERE advert.id = :idAdvert"
			);
			query.setParameter("idAdvert", idAdvert);
			advert = (Advertisement) query.getSingleResult();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return advert;
	}
	
	
	/**
	 * Sélectionne les annonces de l'utilisateur
	 * 
	 * @param idUser : l'id de l'utilisateur dont on va rechercher les annonces
	 * @return List : renvoie une liste contenant les annonces
	 */
	public List<Advertisement> getAllAdvertsOfUser(int idUser) {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
					+ "FROM Advertisement advert "
					+ "WHERE advert.author.id = :idUser "
					+ "AND advert.activated = true"
			);
			query.setParameter("idUser", idUser);
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Sélectionne les annonces en base de données
	 * 
	 * @return List : renvoie une liste contenant les annonces
	 */
	public List<Advertisement> getAllAdverts() {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
					+ "FROM Advertisement advert "
			);
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Désactive une annonce, passant son état "activated" à false
	 * 
	 * @param idAdvert l'id de l'annonce à désactiver
	 */
	public void disableAdvert(int idAdvert) {
		
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			Advertisement advert = session.get(Advertisement.class, idAdvert);
			
			if (advert != null) {
				advert.setActivated(false);
				session.update(advert);
				transact.commit();
			}

		} catch (Exception e) {

		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Supprime une annonce de la base de données
	 * 
	 * @param idAdvert l'id de l'annonce à supprimer
	 */
	public void deleteAdvert(int idAdvert) {
		
		Session session = HibernateUtils.getSession();
		Transaction transact = session.beginTransaction();

		try {
			Advertisement advert = session.get(Advertisement.class, idAdvert);
			
			if (advert != null) {
				session.delete(advert);
				transact.commit();
			}

		} catch (Exception e) {

		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Sélectionne les annonces d'après une recherche par titre
	 * 
	 * @param keyword le mot clé servant à laa recherche
	 * @return List
	 */
	public List<Advertisement> getSearchByTitle(String keyword) {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
				+ "FROM Advertisement advert "
				+ "WHERE LOWER(advert.title) LIKE LOWER(:keyword) "
				+ "AND advert.activated = true"
			);
			query.setParameter("keyword", "%" + keyword + "%");
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Sélectionne les annonces d'après une recherche par genre
	 * 
	 * @param keyword le mot clé servant à laa recherche
	 * @return List
	 */
	public List<Advertisement> getSearchByType(String keyword) {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
				+ "FROM Advertisement advert "
				+ "WHERE LOWER(advert.type) LIKE LOWER(:keyword) "
				+ "AND advert.activated = true"
			);
			query.setParameter("keyword", "%" + keyword + "%");
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Sélectionne les annonces d'après une recherche par ville de l'annonce
	 * 
	 * @param keyword le mot clé servant à laa recherche
	 * @return List
	 */
	public List<Advertisement> getSearchByCity(String keyword) {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
				+ "FROM Advertisement advert "
				+ "WHERE LOWER(advert.author.city) LIKE LOWER(:keyword) "
				+ "AND advert.activated = true"
			);
			query.setParameter("keyword", "%" + keyword + "%");
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}
	
	
	/**
	 * Sélectionne les annonces d'après une recherche par niveau scolaire
	 * 
	 * @param keyword le mot clé servant à laa recherche
	 * @return List
	 */
	public List<Advertisement> getSearchByScholarLevel(String keyword) {

		List<Advertisement> list = new ArrayList<Advertisement>();
		Session session = HibernateUtils.getSession();

		try {
			Query query = session.createQuery(
				"SELECT advert "
				+ "FROM Advertisement advert "
				+ "WHERE LOWER (advert.scholarLevel) LIKE LOWER(:keyword) "
				+ "AND advert.activated = true"
			);
			query.setParameter("keyword", "%" + keyword + "%");
			list = query.getResultList();

		} catch (Exception e) {

		} finally {
			session.close();
		}
		return list;
	}

}