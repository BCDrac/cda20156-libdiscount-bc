package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entité Advertisement, permettant de créer et gérer des annonces.
 * 
 * @author Cécile
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "advertisement")
public class Advertisement {
	
	@Id
	@GenericGenerator(name = "advert_generator", strategy = "org.hibernate.id.enhanced.TableGenerator")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "advert_generator")
	@Column(name = "id")
	private int id;
	
	@Column(nullable = false)
	private String title;
	@Column(nullable = false)
	private String scholarLevel;
	@Column(nullable = false)
	private String isbn;
	@Column(nullable = false)
	private int editionYear;
	@Column(nullable = false)
	private String publisher;
	@Column(nullable = false)
	private String type;
	
	@Column(nullable = false)
	private int quantity;
	private double discount;
	@Column(nullable = false)
	private double unitPrice;
	@Column(nullable = false)
	private double totalPrice;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String comment;
	@ColumnDefault("true")
	private boolean activated;
	
	private String photo1;
	private String photo2;
	private String photo3;
	
	@ManyToOne()
	@JoinColumn(name = "idauthor", referencedColumnName = "id")
	private User author;
}