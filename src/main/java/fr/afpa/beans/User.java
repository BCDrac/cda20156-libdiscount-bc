package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entité User, permettant de créer et gérer des utilisateurs
 * 
 * @author Cécile
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GenericGenerator(name = "user_generator", strategy = "org.hibernate.id.enhanced.TableGenerator")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@Column(name = "id")
	private int id;
	
	@Column(nullable = false)
	private String lastName;
	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false)
	private String libraryName;
	
	@Column (nullable = false)
	private String streetNumber;
	@Column (nullable = false)
	private String streetName;
	private String addressComplement;
	@Column (nullable = false)
	private String city;
	@Column (nullable = false)
	private String postalCode;
	
	@Column(nullable = false, unique = true)
	private String email;
	@Column(nullable = false)
	private String password;
	@Column(nullable = false)
	private String phone;
	@Column(nullable = false)
	private String role;
	@Column(nullable = false)
	private boolean activated;
	
	@OneToMany(mappedBy = "author", cascade = { CascadeType.ALL })
	private List<Advertisement> advertList;
}