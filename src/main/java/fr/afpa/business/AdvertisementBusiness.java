package fr.afpa.business;

import java.util.List;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.dao.AdvertisementDao;

/**
 * Couche métier concernant les annonces
 * 
 * @author Cécile
 */
public class AdvertisementBusiness {
	
	private AdvertisementDao dao;
	
	
	public void createTestAdvert(User user) {
		
		this.dao = new AdvertisementDao();
			
		Advertisement advert = new Advertisement();

		advert.setAuthor(user);
		advert.setTitle("TestTitle");
		advert.setScholarLevel("scholarLevel");
		advert.setType("type");
		advert.setPublisher("publisher");
		advert.setEditionYear(1995);
		advert.setIsbn("1234567890123");
		advert.setQuantity(2);
		advert.setUnitPrice(5);
		advert.setTotalPrice(10);
		advert.setActivated(true);
		
		this.dao.insertAdvertisement(advert);
	}
	
	
	
	/**
	 * Insère une annonce en base de données
	 * 
	 * @param advert l'annonce à insérer
	 * @return boolean : retourne true si l'annonce a bien été insérée
	 */
	public boolean isAdvertCreated(Advertisement advert) {
		this.dao = new AdvertisementDao();
		return this.dao.insertAdvertisement(advert);
	}
	
	
	/**
	 * Récupère une annonce par son id
	 * 
	 * @param idAdvert l'id de l'annonce à récupérer
	 * @return retourne l'annonce trouvée
	 */
	public Advertisement getAdvertByIdActivated(int idAdvert) {
		this.dao = new AdvertisementDao();
		return this.dao.getAdvertByIdActivated(idAdvert);
	}
	
	
	/**
	 * Récupère une annonce par son id
	 * 
	 * @param idAdvert l'id de l'annonce à récupérer
	 * @return retourne l'annonce trouvée
	 */
	public Advertisement getAdvertById(int idAdvert) {
		this.dao = new AdvertisementDao();
		return this.dao.getAdvertById(idAdvert);
	}
	
	
	/**
	 * Récupère toutes les annonces d'un utilisateur
	 * 
	 * @param userId l'id de l'utilisateur
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> getAllAdvertsOfUser(int userId){
		this.dao = new AdvertisementDao();
		return this.dao.getAllAdvertsOfUser(userId);
	}
	
	
	/**
	 * Récupère toutes les annonces en base de données
	 * 
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> getAllAdverts(){
		this.dao = new AdvertisementDao();
		return this.dao.getAllAdverts();
	}
	
	
	/**
	 * Supprime une annonce de la base de données
	 * 
	 * @param idAdvert l'id de l'annonce à supprimer
	 */
	public void deleteAdvert(int idAdvert){
		this.dao = new AdvertisementDao();
		this.dao.deleteAdvert(idAdvert);
	}
	
	
	/**
	 * Désactive une annonce, passant son état "activated" à false
	 * 
	 * @param idAdvert l'id de l'annonce à désactiver
	 */
	public void disableAdvert(int idAdvert){
		this.dao = new AdvertisementDao();
		this.dao.disableAdvert(idAdvert);
	}
	
	
	/**
	 * Met à jour une annonce en base de données
	 * 
	 * @param advert l'annonce à mettre à jour
	 */
	public void updateAdvert(Advertisement advert) {
		this.dao = new AdvertisementDao();
		this.dao.updateAdvertisement(advert);
	}
	
	
	/**
	 * Récupère les annonces d'après une recherche par titre
	 * 
	 * @param keyword le mot-clé servant à la recherche
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> searchByTitle(String keyword){
		this.dao = new AdvertisementDao();
		return this.dao.getSearchByTitle(keyword);
	}
	
	
	/**
	 * Récupère les annonces d'après une recherche par type/genre
	 * 
	 * @param keyword le mot-clé servant à la recherche
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> searchByType(String keyword){
		this.dao = new AdvertisementDao();
		return this.dao.getSearchByType(keyword);
	}
	
	
	/**
	 * Récupère les annonces d'après une recherche par ville de l'auteur
	 * 
	 * @param keyword le mot-clé servant à la recherche
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> searchByCity(String keyword){
		this.dao = new AdvertisementDao();
		return this.dao.getSearchByCity(keyword);
	}
	
	
	/**
	 * Récupère les annonces d'après une recherche par niveau scolaire
	 * 
	 * @param keyword le mot-clé servant à la recherche
	 * @return retourne une liste contenant les annonces trouvées
	 */
	public List<Advertisement> searchByScholarLevel(String keyword){
		this.dao = new AdvertisementDao();
		return this.dao.getSearchByScholarLevel(keyword);
	}

}