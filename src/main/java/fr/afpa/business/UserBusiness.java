package fr.afpa.business;

import java.util.List;

import fr.afpa.beans.User;
import fr.afpa.dao.UserDao;


/**
 * Couche métier concernant l'utilisateur.
 * 
 * @author Cécile
 */
public class UserBusiness {
	
	private UserDao dao;
	
	
	/**
	 * Vérification d'un email.
	 * 
	 * Vérifie, lorsque un utilisateur crée un compte, que l'email entré n'est pas déjà utilisé.
	 * 
	 * @param email l'email à vérifier
	 * @return boolean : return true si l'email est déjà présent en base de données
	 */
	public boolean isEmailUsed(String email) {
		this.dao = new UserDao();
		return dao.isEmailUsed(email);
	}
	
	
	/**
	 * Crée le profil admin
	 */
	public void createAdmin() {
		
		this.dao = new UserDao();
		User admin = new User();
		
		admin.setFirstName("Admin");
		admin.setLastName("Super");	
		admin.setLibraryName("IPV4");
		admin.setStreetNumber("42");
		admin.setStreetName("Open Systems Interconnection");
		admin.setAddressComplement("OSI");
		admin.setCity("WWW");
		admin.setPostalCode("00000");
		
		admin.setEmail("admin@test.com");
		admin.setPassword("password");
		admin.setPhone("1928899255");
		admin.setRole("admin");
		admin.setActivated(true);
		
		this.dao.insertUser(admin);
	}
	
	
	/**
	 * Crée un utilisateur de test
	 * 
	 * @return User : retourne un utilisateur de test
	 */
	public User createTestUser() {
		
		this.dao = new UserDao();
		User user = new User();
		
		user.setFirstName("John");
		user.setLastName("Doe");	
		user.setLibraryName("Libratest");
		user.setStreetNumber("000");
		user.setStreetName("Test Street");
		user.setAddressComplement("This is a test profile");
		user.setCity("Test City");
		user.setPostalCode("00000");
		
		user.setEmail("test@test.com");
		user.setPassword("password");
		user.setPhone("0123456789");
		user.setRole("user");
		user.setActivated(true);
		
		this.dao.insertUser(user);
		return user;
	}
	
	
	/**
	 * Insère un utilisateur en base de données.
	 * 
	 * @param user l'objet utilisateur à insérer en base de données
	 * @return boolean : return true si l'insertion s'est bien effectuée
	 */
	public boolean isRegistered(User user) {
		this.dao = new UserDao();
		return dao.insertUser(user);
	}
	
	
	/**
	 * Récupère un utilisateur d'après l'email et le mot de passe fournis lors de l'identification
	 * 
	 * @param email l'email utilisateur servant pour l'identification
	 * @param password le mot de passe servant pour l'identification
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getUser(String email, String password) {
		this.dao = new UserDao();
		return this.dao.getUser(email, password);
	}
	
	
	/**
	 * Récupère un utilisateur avec le rôle "admin" d'après l'email et le mot de passe fournis lors de l'identification
	 * 
	 * @param email l'email utilisateur servant pour l'identification
	 * @param password le mot de passe servant pour l'identification
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getAdmin(String email, String password) {
		this.dao = new UserDao();
		return this.dao.getAdmin(email, password);
	}
	
	
	/**
	 * Récupère un utilisateur d'après son id
	 * 
	 * @param id l'id servant à récupérer l'utilisateur
	 * @return User : retourne l'utilisateur trouvé
	 */
	public User getUserById(int id) {
		this.dao = new UserDao();
		return this.dao.getUserById(id);
	}
	
	
	/**
	 * Récupère tous les utilisateurs de la base de données
	 * 
	 * @return List : une liste contenant tous les utilisateurs enregistrés sur le site
	 */
	public List<User> getAllUsers() {
		this.dao = new UserDao();
		return this.dao.getAllUsers();
	}
	
	
	/**
	 * Désactive un utilisateur, passant son état "activated" à false.
	 * 
	 * @param idUser l'id utilisateur
	 */
	public void disableUser(int idUser) {
		this.dao = new UserDao();
		this.dao.disableUser(idUser);
	}

}