package fr.afpa.servletsuser;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class UserDisableServlet
 * 
 * Servlet permettant de désactiver un utilisateur
 * 
 * @author Cécile
 */
public class UserDisableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDisableServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String url = "index.jsp";

		if (session != null) {

			int idOther = Integer.parseInt(request.getParameter("idProfile"));
			int id = (int) session.getAttribute("id");

			if (id != 0) {
				this.userBusiness = new UserBusiness();
				User user = this.userBusiness.getUserById(id);
				User userOther = this.userBusiness.getUserById(idOther);
				
				if (user != null && userOther != null) {
					this.userBusiness.disableUser(idOther);
					session.setAttribute("id", user.getId());
					request.setAttribute("user", user);
					request.setAttribute("message", "Utilisateur désactivé.");
					url = "allWebsiteUsers";
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
