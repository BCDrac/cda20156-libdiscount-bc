package fr.afpa.servletsuser;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.User;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class UserRegisterServlet
 * 
 * Servlet gérant l'enregistrement utilisateur
 * 
 * @author Cécile
 */
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegisterServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String url = "index.jsp";

		this.userBusiness = new UserBusiness();
		
		if (this.isFormValid(request)) {
					
			User user = new User();
			
			user.setFirstName(request.getParameter("firstName").trim());
			user.setLastName(request.getParameter("lastName").trim());
			
			user.setLibraryName(request.getParameter("libraryName").trim());
			user.setStreetNumber(request.getParameter("streetNumber").trim());
			user.setStreetName(request.getParameter("streetName").trim());
			user.setAddressComplement(
				request.getParameter("addressComplement").isEmpty()
					? " "
					: request.getParameter("addressComplement").trim()
			);
			user.setCity(request.getParameter("city").trim());
			user.setPostalCode(request.getParameter("postalCode").trim());
			
			user.setEmail(request.getParameter("email").trim());
			user.setPassword(request.getParameter("password").trim());
			user.setPhone(request.getParameter("phone").trim());
			user.setRole("user");
			user.setActivated(true);
			
			if (this.userBusiness.isRegistered(user)) {
				request.setAttribute("message",
						"Compte créé, vous pouvez vous identifier.");
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);				
	}
	
	
	/**
	 * Vérifie la validité du formulaire d'après les autres méthodes de vérification
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isFormValid(HttpServletRequest request) {
		
		return this.isFirstNameValid(request) && this.isLastNameValid(request)
				&& this.isLibraryNameValid(request)	&& this.isStreetNumberValid(request)
				&& this.isStreetNameValid(request) && this.isCityValid(request)
				&& this.isPostalCodeValid(request) && this.isEmailValid(request)
				&& this.isPasswordValid(request) && this.isPhoneValid(request);
	}
	
	
	/**
	 * Vérifie la présence prénom
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isFirstNameValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("firstName") && !request.getParameter("firstName").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du nom
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isLastNameValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("lastName") && !request.getParameter("lastName").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du nom de librairie
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isLibraryNameValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("libraryName") && !request.getParameter("libraryName").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du numéro de rue
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isStreetNumberValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("streetNumber") && !request.getParameter("streetNumber").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du nom de rue
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isStreetNameValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("streetName") && !request.getParameter("streetName").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du nom de ville
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isCityValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("city") && !request.getParameter("city").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence et validité du code postal
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isPostalCodeValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("postalCode") && !request.getParameter("postalCode").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[0-9]{5}$");
	
			matcher = pattern.matcher(request.getParameter("postalCode"));
			return matcher.matches();
		}
		return false;
	}
	
	
	/**
	 * Vérifie la présence et validité de l'email
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isEmailValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("email") && !request.getParameter("email").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("((^[a-z])[\\W\\w]{1,}[^\\W_]@[0-9a-z]+.[a-z]{2,3}$)");
	
			matcher = pattern.matcher(request.getParameter("email"));
			return matcher.matches() && !this.userBusiness.isEmailUsed(request.getParameter("email"));
		}
		return false;
	}
	
	
	/**
	 * Vérifie la présence du mot de passe
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isPasswordValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("password") && !request.getParameter("password").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence et validité du numéro de téléphone
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isPhoneValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("phone") && !request.getParameter("phone").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[0-9]{10}$");
			
			matcher = pattern.matcher(request.getParameter("phone"));
			return matcher.matches();
		}
		return false;
	}
}