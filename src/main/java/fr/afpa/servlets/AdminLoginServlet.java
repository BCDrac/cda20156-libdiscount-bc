package fr.afpa.servlets;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdminLoginServlet
 * 
 * Servlet gérant l'identification de l'administrateur
 * 
 * @author Cécile
 */
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLoginServlet() {
        super();
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "index.jsp";
		
		this.userBusiness = new UserBusiness();
		this.createAdmin(request);
		
		if (this.isLoginValid(request) && this.isPasswordValid(request)) {		
					
			User user = this.userBusiness.getAdmin(request.getParameter("login"), request.getParameter("passwordLogin"));

			if (user == null) {
				request.setAttribute("message", "Échec de la connexion, vérifiez vos identifiants.");
				
			} else if (user != null && user.getRole().equals("admin")) {
				HttpSession session = request.getSession();
				session.setAttribute("id", user.getId());
				request.setAttribute("user", user);
				url = "WEB-INF/Home.jsp";
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	
	
	/**
	 * Vérifie la validité du login
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isLoginValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("login") && !request.getParameter("login").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("((^[a-z])[\\W\\w]{1,}[^\\W_]@[0-9a-z]+.[a-z]{2,3}$)");
	
			matcher = pattern.matcher(request.getParameter("login"));
			return matcher.matches();
		}
		return false;
	}

	
	/**
	 * Vérifie la présence du mot de passe
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isPasswordValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("passwordLogin") && !request.getParameter("passwordLogin").isEmpty();
	}
	
	
	/**
	 * Crée le profil administrateur si celui-ci n'est pas présent en base de données
	 * 
	 * @param request HttpServletRequest
	 */
	private void createAdmin(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("login")
			&& request.getParameter("login").equals("admin@test.com")
			&& request.getParameterMap().containsKey("passwordLogin")
			&& request.getParameter("passwordLogin").equals("password")) {
			
			User admin = this.userBusiness.getUser(request.getParameter("login"), request.getParameter("passwordLogin"));		
			if (admin == null) {
				this.userBusiness.createAdmin();
				User user = this.userBusiness.createTestUser();
				AdvertisementBusiness advertBusiness = new AdvertisementBusiness();
				advertBusiness.createTestAdvert(user);
			}
		}
	}

}