package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class RedirectToAdvertCreateServlet
 * 
 * Servlet de redirection.
 * 
 * @author Cécile
 */
public class RedirectToAdvertCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RedirectToAdvertCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String url = "index.jsp";
		
		if (session != null) {
		
			int id = (int) session.getAttribute("id");
		
			if (id != 0) {
				this.userBusiness = new UserBusiness();
				User user = this.userBusiness.getUserById(id);

				if (user != null) {
					session.setAttribute("id", user.getId());
					request.setAttribute("user", user);
					url = "WEB-INF/CreateAdvert.jsp";					
				}
			}
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}