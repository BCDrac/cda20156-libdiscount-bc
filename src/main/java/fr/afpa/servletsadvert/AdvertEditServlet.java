package fr.afpa.servletsadvert;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertEditServlet
 */
public class AdvertEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
	private AdvertisementBusiness advertBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertEditServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String url = "index.jsp";
		User user = null;
		Advertisement advert = null;
		int idAdvert = Integer.parseInt(request.getParameter("idAdvert"));
		
		if (session != null) {
			int id = (int) session.getAttribute("id");

			if (id != 0) {
				this.userBusiness = new UserBusiness();
				user = this.userBusiness.getUserById(id);
			
				if (user != null && user.getRole().equals("user")) {
					if (idAdvert != 0) {
						this.advertBusiness = new AdvertisementBusiness();
						advert = this.advertBusiness.getAdvertByIdActivated(idAdvert);
						
						if (advert != null && user.getId() == advert.getAuthor().getId()) {			
							session.setAttribute("id", user.getId());
							request.setAttribute("user", user);
							request.setAttribute("advert", advert);
							url = "WEB-INF/CreateAdvert.jsp";
						}
					}
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}