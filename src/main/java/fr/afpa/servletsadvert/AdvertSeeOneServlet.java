package fr.afpa.servletsadvert;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertSeeOneServlet
 * 
 * Servlet permettant d'afficher une annonce
 * 
 * @author Cécile
 */
public class AdvertSeeOneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private UserBusiness userBusiness;  
    private AdvertisementBusiness advertBusiness;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertSeeOneServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String url = "index.jsp";
		User user = null;
		Advertisement advert = null;
		int idAdvert = Integer.parseInt(request.getParameter("idAdvert"));
		
		if (session != null) {
			int id = (int) session.getAttribute("id");

			if (id != 0) {
				this.userBusiness = new UserBusiness();
				user = this.userBusiness.getUserById(id);
			
				if (user != null && user.getRole().equals("user")) {
					if (idAdvert != 0) {
						this.advertBusiness = new AdvertisementBusiness();
						advert = this.advertBusiness.getAdvertByIdActivated(idAdvert);
						
						if (advert != null) {
							session.setAttribute("id", user.getId());
							request.setAttribute("user", user);
							request.setAttribute("advert", advert);
							url = "WEB-INF/SeeAdvert.jsp";
						}
					}
				} else if (user != null && user.getRole().equals("admin")) {
					if (idAdvert != 0) {
						this.advertBusiness = new AdvertisementBusiness();
						advert = this.advertBusiness.getAdvertById(idAdvert);
						
						if (advert != null) {
							session.setAttribute("id", user.getId());
							request.setAttribute("user", user);
							request.setAttribute("advert", advert);
							request.setAttribute("idAdvert", advert.getId());
							url = "WEB-INF/SeeAdvert.jsp";
						}
					}
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}