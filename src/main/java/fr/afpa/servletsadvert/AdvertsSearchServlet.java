package fr.afpa.servletsadvert;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertsSearchServlet
 * 
 * Servlet permettant la recherche d'annonce
 * 
 * @author Cécile
 */
public class AdvertsSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
	private AdvertisementBusiness advertBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertsSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session = request.getSession();
		String url = "index.jsp";
		User user = null;

		if (session != null) {

			int id = (int) session.getAttribute("id");

			if (id != 0) {
				this.userBusiness = new UserBusiness();
				user = this.userBusiness.getUserById(id);				
				this.advertBusiness = new AdvertisementBusiness();
				
				if (user != null) {
					
					if (request.getParameter("searchOption").equals("searchByTitle")) {
						request.setAttribute("advertList", this.searchByTitle(request.getParameter("searchAdvert")));
						url = "WEB-INF/SeeResults.jsp";
						
					} else if (request.getParameter("searchOption").equals("searchByType")) {
						request.setAttribute("advertList", this.searchByType(request.getParameter("searchAdvert")));
						url = "WEB-INF/SeeResults.jsp";
						
					} else if (request.getParameter("searchOption").equals("searchByCity")) {
						request.setAttribute("advertList", this.searchByCity(request.getParameter("searchAdvert")));
						url = "WEB-INF/SeeResults.jsp";
						
					} else if (request.getParameter("searchOption").equals("searchByScholarLevel")) {
						request.setAttribute("advertList", this.searchByScholarLevel(request.getParameter("searchAdvert")));
						url = "WEB-INF/SeeResults.jsp";
					}
					
				}
				
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);			
	}

	
	/**
	 * Recherche une annonce par titre
	 * 
	 * @param keyword le mot clé servant à la recherche
	 * @return List : une list contenant les annonces trouvées
	 */
	private List<Advertisement> searchByTitle(String keyword){
		return this.advertBusiness.searchByTitle(keyword);
	}
	
	
	/**
	 * Recherche une annonce par type/genre
	 * 
	 * @param keyword le mot clé servant à la recherche
	 * @return List : une list contenant les annonces trouvées
	 */
	private List<Advertisement> searchByType(String keyword){
		return this.advertBusiness.searchByType(keyword);
	}
	
	
	/**
	 * Recherche une annonce par ville
	 * 
	 * @param keyword le mot clé servant à la recherche
	 * @return List : une list contenant les annonces trouvées
	 */
	private List<Advertisement> searchByCity(String keyword){
		return this.advertBusiness.searchByCity(keyword);
	}
	
	
	/**
	 * Recherche une annonce par niveau scolaire
	 * 
	 * @param keyword le mot clé servant à la recherche
	 * @return List : une list contenant les annonces trouvées
	 */
	private List<Advertisement> searchByScholarLevel(String keyword){
		return this.advertBusiness.searchByScholarLevel(keyword);
	}

}