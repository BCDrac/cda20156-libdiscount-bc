package fr.afpa.servletsadvert;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertsSeeAllServlet
 * 
 * Servlet permettant à l'utilisateur de récupérer toutes les annonces
 * 
 * @author Cécile
 */
public class AdvertsSeeAllServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
	private AdvertisementBusiness advertBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertsSeeAllServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String url = "index.jsp";
		
		if (session != null) {
		
			int id = (int) session.getAttribute("id");
			
			if (id != 0) {
				this.userBusiness = new UserBusiness();
				User user = this.userBusiness.getUserById(id);
				
				if (user != null) {
					this.advertBusiness = new AdvertisementBusiness();
					session.setAttribute("id", user.getId());
					request.setAttribute("user", user);
					request.setAttribute("advertList", this.advertBusiness.getAllAdvertsOfUser(id));		
					url = "WEB-INF/allMyAdverts.jsp";
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}