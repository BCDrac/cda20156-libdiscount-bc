package fr.afpa.servletsadvert;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Advertisement;
import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertCreateServlet
 * 
 * Servlet de création d'annonce
 * 
 * @author Cécile
 */
public class AdvertCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
	private AdvertisementBusiness advertBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertCreateServlet() {
        super();
    }

    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String url = "index.jsp";

		if (session != null) {

			int id = (int) session.getAttribute("id");

			if (id != 0) {
				this.userBusiness = new UserBusiness();
				User user = this.userBusiness.getUserById(id);
				Advertisement advert = null;

				if (user != null) {
					
					if (this.isTitleValid(request) && this.isTypeValid(request) && this.isPublisherValid(request)
							&& this.isEditionYearValid(request) && this.isIsbnValid(request)
							&& this.isScholarLevelValid(request) && this.isQuantityValid(request)
							&& this.isUnitPriceValid(request)) {

						advert = new Advertisement();

						advert.setAuthor(user);
						advert.setTitle(request.getParameter("title").trim());
						advert.setScholarLevel(request.getParameter("scholarLevel").trim());
						advert.setType(request.getParameter("type").trim());
						advert.setPublisher(request.getParameter("publisher").trim());
						advert.setEditionYear(Integer.parseInt(request.getParameter("editionYear").trim()));
						advert.setIsbn(request.getParameter("isbn").trim());
						advert.setQuantity(Integer.parseInt(request.getParameter("quantity").trim()));
						advert.setUnitPrice(Double.parseDouble(request.getParameter("unitPrice").trim()));
						advert.setTotalPrice(this.calculateTotalPrice(request));
						advert.setActivated(true);

						if (this.isDiscountValid(request)) {
							advert.setDiscount(Double.parseDouble(request.getParameter("discount").trim()));
						}

						if (!request.getParameter("comment").trim().isEmpty()) {
							advert.setComment(request.getParameter("comment").trim());
						}

						this.advertBusiness = new AdvertisementBusiness();

						if (this.advertBusiness.isAdvertCreated(advert)) {
							session.setAttribute("id", user.getId());
							request.setAttribute("message", "Annonce créée !");
							request.setAttribute("user", user);
							request.setAttribute("advert", advert);
							url = "WEB-INF/SeeAdvert.jsp";
						}
					}
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	
	
	/**
	 * Vérifie la présence du titre
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isTitleValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("title") && !request.getParameter("title").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence du type/genre littéraire
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isTypeValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("type") && !request.getParameter("type").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence de l'éditeur
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isPublisherValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("publisher") && !request.getParameter("publisher").isEmpty();
	}

	
	/**
	 * Vérifie la présence et validité de la date d'édition
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isEditionYearValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("editionYear") && !request.getParameter("editionYear").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[^0][0-9]{0,3}$");
	
			matcher = pattern.matcher(request.getParameter("editionYear"));
			return matcher.matches();
		}
		return false;
	}
	
	
	/**
	 * Vérifie la présence et validité de l'isbn
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isIsbnValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("isbn") && !request.getParameter("isbn").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[0-9]{10,13}$");
	
			matcher = pattern.matcher(request.getParameter("isbn"));
			return matcher.matches();
		}
		return false;
	}
	
	
	/**
	 * Vérifie la présence du niveau scholaire
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isScholarLevelValid(HttpServletRequest request) {
		return request.getParameterMap().containsKey("scholarLevel") && !request.getParameter("scholarLevel").isEmpty();
	}
	
	
	/**
	 * Vérifie la présence et validité de la quantité
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isQuantityValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("quantity") && !request.getParameter("quantity").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[^0][0-9]{0,9}$");
	
			matcher = pattern.matcher(request.getParameter("quantity"));
			return matcher.matches();
		}
		return false;
	}
	
	
	/**
	 * Vérifie la présence et validité du prix unitaire
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isUnitPriceValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("unitPrice") && !request.getParameter("unitPrice").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[1-9][0-9]{0,9}(\\.[0-9]{2})?$");
	
			matcher = pattern.matcher(request.getParameter("unitPrice"));
			return matcher.matches();
		}
		return false;
	}
	
	
	/**
	 * Calcule le prix total
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private Double calculateTotalPrice(HttpServletRequest request) {
		
		Double totalPrice = 0.0;
		
		if (this.isUnitPriceValid(request) && this.isDiscountValid(request)) {			
			Double unitPrice = Double.parseDouble(request.getParameter("unitPrice").trim());
			Double discount = Double.parseDouble(request.getParameter("discount").trim());
			int quantity = Integer.parseInt(request.getParameter("quantity").trim());
			totalPrice = unitPrice * quantity;
			discount = totalPrice * discount / 100;
			totalPrice -= discount;
			
		} else {
			Double unitPrice = Double.parseDouble(request.getParameter("unitPrice").trim());
			int quantity = Integer.parseInt(request.getParameter("quantity").trim());
			totalPrice = unitPrice * quantity;
		}
		return totalPrice;
	}
	
	
	/**
	 * Vérifie la présence et validité de la remise
	 * 
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isDiscountValid(HttpServletRequest request) {
		if (request.getParameterMap().containsKey("discount") && !request.getParameter("discount").isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[1-9][0-9]{0,9}(\\.[0-9]{2})?$");
	
			matcher = pattern.matcher(request.getParameter("discount"));
			return matcher.matches();
		}
		return false;
	}
	
}