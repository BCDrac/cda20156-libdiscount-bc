package fr.afpa.servletsadvert;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.User;
import fr.afpa.business.AdvertisementBusiness;
import fr.afpa.business.UserBusiness;

/**
 * Servlet implementation class AdvertsSeeAllWebsiteServlet
 * 
 * Servlet permettant de récupérer toutes les annonces du site
 * 
 * @author Cécile
 */
public class AdvertsSeeAllWebsiteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserBusiness userBusiness;
	private AdvertisementBusiness advertBusiness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertsSeeAllWebsiteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String url = "index.jsp";
		
		if (session != null) {
		
			int id = (int) session.getAttribute("id");
			
			if (id != 0) {
				this.userBusiness = new UserBusiness();
				User user = this.userBusiness.getUserById(id);
				
				if (user != null) {
					this.advertBusiness = new AdvertisementBusiness();
					session.setAttribute("id", user.getId());
					request.setAttribute("user", user);
					request.setAttribute("advertList", this.advertBusiness.getAllAdverts());		
					url = "WEB-INF/allWebsiteAdverts.jsp";
				}
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}