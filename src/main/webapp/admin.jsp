<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue admin</title>
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <header>
		<img id="banner" src="images/banner.jpg">
	</header>
	<body>
		<main>
			<form method="post" action="adminLogin" id="formLogin">
				<label>Email : </label>
				<input type="email" name="login" value="<c:out value="${param.login}"/>" required>
				<label>Mot de passe : </label>
				<input type="password" name="passwordLogin" value="<c:out value="${param.passwordLogin}"/>" required>			
				<input type="submit" value="Connexion">
			</form>
		</main>
	</body>
</html>