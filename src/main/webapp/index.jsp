<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue</title>
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <header>
		<img id="banner" src="images/banner.jpg">
	</header>
	<body>
		<main>
		
			<c:if test="${!empty message}">
				<p id="noAdvertCreated"><c:out value="${message}"></c:out></p>
			</c:if>
		
			<form method="post" action="home" id="formLogin">
				<label>Email : </label>
				<input type="email" name="login" value="<c:out value="${param.login}"/>" required>
				<label>Mot de passe : </label>
				<input type="password" name="passwordLogin" value="<c:out value="${param.passwordLogin}"/>" required>			
				<input type="submit" value="Connexion">
			</form>
	
			<br/>
	
			<form method="post" action="register" id="formRegister">
				<label>Nom : </label>
				<input type="text" name="lastName" value="<c:out value="${param.lastName}"/>" required>
				<label>Prénom : </label>
				<input type="text" name="firstName" value="<c:out value="${param.firstName}"/>" required>
				<br/>
				<label>Nom de la librairie : </label>
				<input type="text" name="libraryName" value="<c:out value="${param.libraryName}"/>" required>
				<label>Numéro de rue : </label>
				<input type="text" name="streetNumber" value="<c:out value="${param.streetNumber}"/>" required>
				<label>Nom de la rue : </label>
				<input type="text" name="streetName" value="<c:out value="${param.streetName}"/>" required>
				<label>Complément d'adresse : </label>
				<input type="text" name="addressComplement" value="<c:out value="${param.addressComplement}"/>">
				<label>Ville : </label>
				<input type="text" name="city" value="<c:out value="${param.city}"/>" required>
				<label>Code postal : </label>
				<input type="text" name="postalCode" maxlength="5" pattern="^[0-9]{5}$" value="<c:out value="${param.postalCode}"/>" required>
				<br/>
				<label>Email de contact : </label>
				<input type="email" name="email" value="<c:out value="${param.email}"/>" required>
				<label>Téléphone : </label>
				<input type="tel" name="phone" maxlength="10" pattern="^[0-9]{10}$" value="<c:out value="${param.phone}"/>" required>
				<label>Entrez un mot de passe : </label>
				<input type="password" name="password" value="<c:out value="${param.password}"/>" required>
				<label>Entrez à nouveau votre mot de passe : </label>
				<input type="password" name="passwordVerif" value="<c:out value="${param.passwordVerif}"/>" required>
				
				<input type="submit" value="Créer un compte">
			</form>
		</main>
	</body>
</html>