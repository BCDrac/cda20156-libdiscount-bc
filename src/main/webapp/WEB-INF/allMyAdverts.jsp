<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Mes annonces</title>
	<link rel="stylesheet" href="styles/style.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	
	<body>
		<c:if test="${!empty message}">
			<c:out value="${message}"/>
		</c:if>
	
		<c:if test="${empty advertList}">
			<p id="noAdvertCreated">Vous n'avez pas encore créé d'annonce.</p>
		</c:if>
	
		<c:forEach items="${advertList}" var="advert">
		<div class="advertList">
			<div>
				<p><span>Titre : </span><c:out value="${advert.title}"/></p>
				<p><span>Maison d'édition : </span><c:out value="${advert.publisher}"/></p>
				<p><span>Année d'édition : </span><c:out value="${advert.editionYear}"/></p>
			</div>
			<div>
				<p><span>Quantité : </span><c:out value="${advert.quantity}"/></p>
				<p><span>Prix unitaire : </span><c:out value="${advert.unitPrice}"/>€</p>
				<p><span>Prix total : </span><c:out value="${advert.totalPrice}"/>€</p>
			</div>
			<span>
				<a href="<c:url value="seeAdvert"><c:param name="idAdvert" value="${advert.id}"/></c:url>">Voir l'annonce</a>
			</span>
		</div>
		</c:forEach>

	</body>
</html>