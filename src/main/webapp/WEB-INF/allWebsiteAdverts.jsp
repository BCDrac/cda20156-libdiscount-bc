<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Toutes les annonces</title>
	<link rel="stylesheet" href="styles/style.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	
	<body>

		<div><c:out value="${message}"/></div>
	
		<c:if test="${empty advertList}">
			<p id="noAdvertCreated">Aucune annonce créée pour le moment.</p>
		</c:if>
	
		<c:forEach items="${advertList}" var="advert">
		<div class="advertList">
				<p><span>Titre : </span><c:out value="${advert.title}"/></p>
				<p><span>Maison d'édition : </span><c:out value="${advert.publisher}"/></p>
				<p><span>Année d'édition : </span><c:out value="${advert.editionYear}"/></p>

				<p><span>Quantité : </span><c:out value="${advert.quantity}"/></p>
				<p><span>Prix unitaire : </span><c:out value="${advert.unitPrice}"/></p>
				<p><span>Prix total : </span><c:out value="${advert.totalPrice}"/></p>

				<c:choose>
				     <c:when test="${!advert.activated}" >
				         <p><span>État : </span>désactivé</p>
				     </c:when>
				     <c:otherwise>
				         <p><span>État : </span>activé</p>
				     </c:otherwise>
				 </c:choose>
			<span>
				<a href="<c:url value="seeAdvert"><c:param name="idAdvert" value="${advert.id}"/></c:url>">Voir l'annonce</a>
			</span>
		</div>
		</c:forEach>

	</body>
</html>