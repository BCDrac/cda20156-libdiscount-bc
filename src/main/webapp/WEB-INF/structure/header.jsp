<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header>
	<img id="banner" src="images/banner.jpg">
	<div id="navbar">
		<span>Bienvenue
		<c:out value="${user.firstName}"></c:out>
		<c:out value="${user.lastName}"></c:out></span>
		<span><a href="profile">Voir le profil</a></span>
		<span><a href="disconnect">Se déconnecter</a></span>
	</div>
	
	<div id="searchbar">
		<span><a href="toAdvertCreate">Créer une annonce</a></span>
		<c:choose>
		     <c:when test="${user.role == 'admin'}" >
		         <span><a href="allWebsiteAdverts">Voir toutes les annonces du site</a></span>
		         <span><a href="allWebsiteUsers">Voir tous les utilisateurs du site</a></span>
		     </c:when>
		     <c:otherwise>
		         <span><a href="allMyAdverts">Voir toutes mes annonces</a></span>
		     </c:otherwise>
		 </c:choose>
		
		<form action="searchAdverts" method="get">
			<span>Rechercher une annonce par : 
				<select name="searchOption">
					<option value="searchByTitle">Mot clé de titre</option>
					<option value="searchByType">Type de produit</option>
					<option value="searchByCity">Ville de l'annonce</option>
					<option value="searchByScholarLevel">Niveau scolaire</option>
				</select>
				<input type="text" name="searchAdvert">
				<input type="submit" value="Rechercher">
			</span>
		</form>
		<span><a href="toHome">Retour à l'accueil</a></span>
	</div>
	
</header>