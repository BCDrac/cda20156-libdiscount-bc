<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Profil</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/profile.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	
	<body>
	
		<div id="profile">
			<p><span>Nom : </span><c:out value="${user.lastName}"></c:out></p>
			<p><span>Prénom : </span><c:out value="${user.firstName}"></c:out></p>
			<p><span>Nom de la librairie : </span><c:out value="${user.libraryName}"></c:out></p>
			<p><span>Numéro : </span><c:out value="${user.streetNumber}"></c:out></p>
			<p><span>Rue : </span><c:out value="${user.streetName}"></c:out></p>
			<p><span>Complément : </span><c:out value="${user.addressComplement}"></c:out></p>
			<p><span>Ville : </span><c:out value="${user.city}"></c:out></p>
			<p><span>Code postal : </span><c:out value="${user.postalCode}"></c:out></p>
			<p><span>Email : </span><c:out value="${user.email}"></c:out></p>
			<p><span>Téléphone : </span><c:out value="${user.phone}"></c:out></p>
		</div>
	
	</body>
</html>