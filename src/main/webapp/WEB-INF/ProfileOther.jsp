<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Profil</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/profile.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	
	<body>
	
		<div id="profile">
			<p><span>Nom : </span><c:out value="${userOther.lastName}"></c:out></p>
			<p><span>Prénom : </span><c:out value="${userOther.firstName}"></c:out></p>
			<p><span>Nom de la librairie : </span><c:out value="${userOther.libraryName}"></c:out></p>
			<p><span>Numéro : </span><c:out value="${userOther.streetNumber}"></c:out></p>
			<p><span>Rue : </span><c:out value="${userOther.streetName}"></c:out></p>
			<p><span>Complément : </span><c:out value="${userOther.addressComplement}"></c:out></p>
			<p><span>Ville : </span><c:out value="${userOther.city}"></c:out></p>
			<p><span>Code postal : </span><c:out value="${userOther.postalCode}"></c:out></p>
			<p><span>Email : </span><c:out value="${userOther.email}"></c:out></p>
			<p><span>Téléphone : </span><c:out value="${userOther.phone}"></c:out></p>
			<c:choose>
				<c:when test="${!userOther.activated}">
					<p><span>État : </span>désactivé</p>
				</c:when>
				<c:otherwise>
					<p><span>État : </span>activé</p>
				</c:otherwise>
			</c:choose>
			
			<c:if test="${user.role.equals('admin') && userOther.activated}">
				<a href="<c:url value="disableUser"><c:param name="idProfile" value="${userOther.id}"/></c:url>">Désactiver l'utilisateur</a>
			</c:if>
		</div>
	
	</body>
</html>