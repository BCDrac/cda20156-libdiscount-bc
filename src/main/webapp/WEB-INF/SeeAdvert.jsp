<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Aperçu de l'annonce</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/advert.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>

	<body>
		<c:if test="${!empty message}">
			<c:out value="${message}"/>
		</c:if>

		<div id="advertCard">
			<p><span>Librairie : </span><c:out value="${advert.author.libraryName}"></c:out></p>
			<p><span>Ville : </span><c:out value="${advert.author.city}"></c:out></p>
			<p><span>Code postal : </span><c:out value="${advert.author.postalCode}"></c:out></p>

			<p><span>Titre : </span><c:out value="${advert.title}"></c:out></p>
			<p><span>Maison d'édition : </span><c:out value="${advert.publisher}"></c:out></p>
			<p><span>Année d'édition : </span><c:out value="${advert.editionYear}"></c:out></p>
			<p><span>ISBN : </span><c:out value="${advert.isbn}"></c:out></p>

			<p><span>Niveau d'étude : </span><c:out value="${advert.scholarLevel}"></c:out></p>
			<p><span>Quantité : </span><c:out value="${advert.quantity}"></c:out></p>
			<p><span>Prix unitaire : </span><c:out value="${advert.unitPrice}"></c:out>€</p>
			<p><span>Remise : </span><c:out value="${advert.discount}"></c:out>%</p>
			<p><span>Prix total : </span><c:out value="${advert.totalPrice}"></c:out>€</p>

			<p><c:out value="${advert.comment}"></c:out></p>
			
			<c:choose>
				<c:when test="${!advert.activated}">
					<p><span>État : </span>désactivé</p>
				</c:when>
				<c:otherwise>
					<p><span>État : </span>activé</p>
				</c:otherwise>
			</c:choose>

			<c:choose>
			     <c:when test="${advert.author.id == sessionScope.id}" >
				     <span>
						<a href="<c:url value="editAdvert"><c:param name="idAdvert" value="${advert.id}"/></c:url>">Modifier l'annonce</a>
					</span>
					 <span>
						<a href="<c:url value="disableAdvert"><c:param name="idAdvert" value="${advert.id}"/></c:url>">Désactiver l'annonce</a>
					</span>
			     </c:when>
			     <c:when test="${user.role == 'admin'}">
			     	 <span>
						<a href="<c:url value="deleteAdvert"><c:param name="idAdvert" value="${advert.id}"/></c:url>">Supprimer l'annonce</a>
					</span>
			     </c:when>
			 </c:choose>
		</div>

	</body>
</html>