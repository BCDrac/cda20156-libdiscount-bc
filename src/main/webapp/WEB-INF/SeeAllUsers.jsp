<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Liste des profils</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/profile.css">
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	
	<body>
		<c:if test="${!empty message}">
			<c:out value="${message}"/>
		</c:if>
	
		<c:if test="${empty userList}">
			<p id="noRegisteredUser">Aucun utilisateur enregistré sur le site.</p>
		</c:if>
	
		<c:forEach items="${userList}" var="webUser">
			<div class="userList">
				<p><span>Nom : </span><c:out value="${webUser.lastName}"></c:out></p>
				<p><span>Prénom : </span><c:out value="${webUser.firstName}"></c:out></p>
				<p><span>Nom de la librairie : </span><c:out value="${webUser.libraryName}"></c:out></p>
				<p><span>Ville : </span><c:out value="${webUser.city}"></c:out></p>
				<p><span>Code postal : </span><c:out value="${webUser.postalCode}"></c:out></p>
				<p><span>Email : </span><c:out value="${webUser.email}"></c:out></p>
				<c:choose>
				     <c:when test="${!webUser.activated}" >
				         <p><span>État : </span>désactivé</p>
				     </c:when>
				     <c:otherwise>
				         <p><span>État : </span>activé</p>
				     </c:otherwise>
				 </c:choose>
				<a href="<c:url value="seeOtherProfile"><c:param name="idProfile" value="${webUser.id}"/></c:url>">Voir le profil</a>
			</div>
		</c:forEach>
	
	</body>
</html>