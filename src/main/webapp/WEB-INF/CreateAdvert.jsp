<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<c:choose>
		<c:when test="${advert != null}" >
			<title>Modifier une annonce</title>
		</c:when>
		<c:otherwise>
			<title>Créer une annonce</title>
		</c:otherwise>
	</c:choose>

	<link rel="stylesheet" href="styles/style.css">
	</head>

	<jsp:include page="structure/header.jsp"/>

<body>

	<form action="${advert != null ? 'advertUpdate' : 'advertCreate'}" method="post">
		<label>Titre : </label>
		<input type="text" name="title" value="${advert != null ? advert.title : null}" required>
		<label>Genre : </label>
		<input type="text" name="type" value="${advert != null ? advert.type : null}" required>
		<label>Maison d'édition : </label>
		<input type="text" name="publisher" value="${advert != null ? advert.publisher : null}" required>
		<label>Année d'édition : </label>
		<input type="number" name="editionYear" value="${advert != null ? advert.editionYear : null}" pattern="^[^0][0-9]{0,3}$" required>
		<label>ISBN : </label>
		<input type="text" name="isbn" value="${advert != null ? advert.isbn : null}" pattern="^[0-9]{10,13}$" maxlength="13" required>
		<label>Niveau scolaire : </label>
		<input type="text" name="scholarLevel" value="${advert != null ? advert.scholarLevel : null}" >
		<br/>
		<label>Quantité : </label>
		<input type="number" name="quantity" value="${advert != null ? advert.quantity : null}" pattern="^[^0][0-9]{0,9}$" required>
		<label>Prix unitaire : </label>
		<input type="text" name="unitPrice" value="${advert != null ? String.format('%,.2f', advert.unitPrice) : null}" pattern="^[1-9][0-9]{0,9}(\\.[0-9]{2})?$" required>
		<label>Remise (sur prix total) : </label>
		<input type="text" name="discount" value="${advert != null ? String.format('%,.2f', advert.discount) : null}" pattern="^[1-9][0-9]{0,9}(\\.[0-9]{2})?$">%
		<br/>
		<label>Commentaire : </label>
		<input type="text" name="comment" value="${advert != null ? advert.comment : null}">
		<c:if test="${advert != null}">
			<input type="hidden" name="idAdvert" value="${advert.id}">
		</c:if>
		<input type="submit" value="${advert != null ? 'Modifier l\'annonce' : 'Créer l\'annonce'}">
	</form>
	</body>
</html>