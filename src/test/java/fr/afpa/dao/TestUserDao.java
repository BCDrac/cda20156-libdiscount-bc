package fr.afpa.dao;

import java.util.List;

import org.junit.Test;

import fr.afpa.beans.User;
import junit.framework.TestCase;

public class TestUserDao extends TestCase {
	
	UserDao dao;
	
	public void setUp() throws Exception {
		
		super.setUp();
		this.dao = new UserDao();	
	}

	@Test
	public void testIsEmailUsed() {
		assertTrue(this.dao.isEmailUsed("test@test.com"));
		assertFalse(this.dao.isEmailUsed("test@mail.com"));
	}

	@Test
	public void testGetUser() {
		User user = this.dao.getUser("test@test.com", "password");
		assertTrue(user != null);
	}

	@Test
	public void testGetAdmin() {
		User admin = this.dao.getAdmin("admin@test.com", "password");
		assertTrue(admin != null);
	}

	@Test
	public void testGetUserById() {
		User user = this.dao.getUserById(2);
		assertTrue(user != null);
	}

	@Test
	public void testGetAllUsers() {
		List<User> userList = this.dao.getAllUsers();
		assertTrue(!userList.isEmpty());
	}

}