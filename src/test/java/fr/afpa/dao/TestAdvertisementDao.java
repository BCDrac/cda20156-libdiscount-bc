package fr.afpa.dao;

import java.util.List;

import org.junit.Test;

import fr.afpa.beans.Advertisement;
import junit.framework.TestCase;

public class TestAdvertisementDao extends TestCase {

	AdvertisementDao dao;
	
	public void setUp() throws Exception {
		
		super.setUp();
		this.dao = new AdvertisementDao();	
	}
	
	@Test
	public void testGetAdvertByIdActivated() {
		Advertisement advert = this.dao.getAdvertByIdActivated(1);
		assertTrue(advert != null);
	}

	@Test
	public void testGetAdvertById() {
		Advertisement advert = this.dao.getAdvertById(1);
		assertTrue(advert != null);
	}

	@Test
	public void testGetAllAdverts() {
		List <Advertisement> advertList = this.dao.getAllAdverts();
		assertTrue(advertList != null);
	}

	@Test
	public void testGetSearchByTitle() {
		List <Advertisement> advertList = this.dao.getSearchByTitle("Test");
		assertTrue(advertList != null);
	}

	@Test
	public void testGetSearchByType() {
		List <Advertisement> advertList = this.dao.getSearchByType("type");
		assertTrue(advertList != null);
	}

	@Test
	public void testGetSearchByCity() {
		List <Advertisement> advertList = this.dao.getSearchByCity("Test");
		assertTrue(advertList != null);
	}

	@Test
	public void testGetSearchByScholarLevel() {
		List <Advertisement> advertList = this.dao.getSearchByScholarLevel("scholar");
		assertTrue(advertList != null);
	}

}