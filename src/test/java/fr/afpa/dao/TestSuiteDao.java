package fr.afpa.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestUserDao.class, TestAdvertisementDao.class })
public class TestSuiteDao {

}
